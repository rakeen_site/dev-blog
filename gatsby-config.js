module.exports = {
  siteMetadata: {
    title: `dev blog`,
    author: `Shadman Rakeen`,
    GA: process.env.GA || `UA-70289103-1`,
    siteUrl: 'https://blog.rakeen.gq'
  },

  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages/posts/`,
        name: "posts"
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 690,
            },
          },
          {
            resolve: "gatsby-remark-embed-video",
            options: {
              width: 800,
              ratio: 1.77, // Optional: Defaults to 16/9 = 1.77
              height: 400, // Optional: Overrides optional.ratio
              related: false, //Optional: Will remove related videos from the end of an embedded YouTube video.
              noIframeBorder: true //Optional: Disable insertion of <style> border: 0
            }
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
          },
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              classPrefix: 'language-',
            },
          },
          `gatsby-remark-autolink-headers`,
          `gatsby-remark-katex`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    {
      resolve: 'gatsby-remark-embed-snippet',
      options: {
        classPrefix: 'language-',
        // Example code links are relative to this dir.
        // eg pages/path/to/file.js
        directory: `${__dirname}/src/pages`,
      },
    },
    {
      // need to set process.env.NODE_ENV === `production
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId:  process.env.GA || `UA-70289103-1`,
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "dev blog",
        short_name: "dev-blog",
        icons: [
          {
            src: "/static/favicon.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
        start_url: "/",
        background_color: "white",
        theme_color: "white",
        display: "minimal-ui",
      },
    },
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://blog.rakeen.gq`,
      }
    },
    {
      resolve: `gatsby-plugin-nprogress`,
      options: {
        color: `tomato`,
      }
    },
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`
  ],
}

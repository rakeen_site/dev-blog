import React from 'react';
import Helmet from 'react-helmet';
import { TypographyStyle, GoogleFont } from 'react-typography';
import typography from './utils/typography';

const BUILD_TIME = new Date().getTime();

module.exports = React.createClass({
  propTypes () {
    return {
      body: React.PropTypes.string,
    }
  },
  render () {
    const head = Helmet.rewind()

    let css
    if (process.env.NODE_ENV === 'production') {
      css = <style dangerouslySetInnerHTML={{ __html: require('!raw!../public/styles.css') }} />
    }

    return (
      <html lang="en">
        <head>
          <meta charSet="utf-8" />

          <link rel="dns-prefetch" href="https://cdnjs.cloudflare.com/" />

          <meta name="theme-color" content="#4CAF50" />
          <link type="text/plain" rel="author" href="http://humanstxt.org/humans.txt" />
          <link rel="icon" href="/favicon.png" sizes="512x512" type="image/png" />
          <meta name="generator" content="gatsby" />

          <meta name="author" content="Shadman Rakeen" />
          <meta name="contact" content="ping@rakeen.gq" />
          <meta name="google-site-verification" content="DIvz_xNcL_PEq1yQ9RCCDPvbarRU60fJVNl9G3-XSuU" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          {head.title.toComponent()}
          {head.link.toComponent()}
          {head.meta.toComponent()}
          {head.script.toComponent()}
          <TypographyStyle typography={typography} />
          <GoogleFont typography={typography} />
          {css}
          {this.props.headComponents}
          {/* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.5.1/katex.min.css" /> */}
        </head>
        <body>
          <div id="___gatsby" dangerouslySetInnerHTML={{ __html: this.props.body }} />
          {/* <script src={prefixLink(`/bundle.js?t=${BUILD_TIME}`)} /> */}
          {this.props.postBodyComponents}
        </body>
      </html>
    )
  },
})

import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'react-responsive-grid';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Headroom from 'react-headroom';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import {rhythm} from '../utils/typography';
import '../css/styles.css';
//import '../css/markdown-styles.css';

const TemplateWrapper = ({ children, location, route }) => (
  <div>
    {location.pathname!=="/" &&
    <Headroom
      wrapperStyle={{
        marginBottom: rhythm(1),
      }}
      style={{
        background: '#f8f8f8',
      }}
    >
      <Container
        style={{
          maxWidth: 960,
          paddingTop: 0,
          padding: `${rhythm(1)} ${rhythm(3/4)}`,
        }}
      >
        <Link
          to={'/'}
          style={{
            color: 'black',
            textDecoration: 'none',
          }}
        >
          dev-blog
        </Link>
      </Container>
    </Headroom>
    }
    {location.pathname==="/" &&
      <Container
        style={{
          maxWidth: 960,
          paddingTop: 0,
          padding: `${rhythm(1)} ${rhythm(3/4)}`,
        }}
      >
        <Helmet
          meta = {[
            { "property": "og:image", content: "https://cldup.com/7LNcrB9Kh5.jpg" },
          ]}
        />
        <center>
          <div className="avatar" style={{
            width: '120px',
            height: '120px',
            borderRadius: '50%',
            backgroundImage: "url('https://cldup.com/7LNcrB9Kh5.jpg')",
            backgroundSize: '120px'
          }}></div>

        <h1 style={{ margin: '50px 20px', fontWeight: '700' }}>dev blog</h1>
        </center>
      </Container>
    }
    {/*
      720px or you could do `65ch` which is 65 characters in width
      ref: https://twitter.com/notwaldorf/status/866481093813325824
    */}
    <Container
      style={{
        maxWidth: 720,
        padding: `${rhythm(1)} ${rhythm(3/4)}`,
        paddingTop: 0,
      }}
    >
      {children()}
      <footer
        style={{
          color: 'black',
          fontSize: '8px',
          textAlign: 'right',
        }}
      >
        <span style={{color: 'red'}}>❤</span> rakeen
      </footer>
    </Container>
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.any,
  location: PropTypes.object,
  route: PropTypes.object
};

export default TemplateWrapper;

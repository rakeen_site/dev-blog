---
title: testing markdown  
date: 6-Jan-2018
path: /test/  
tags:
  - markdown
  - test
  - cheat sheet
---


This is a GFM todo list:  

- [ ] this is number one task  
- [x] this is second task which is done 😄  


```javascript{1,3-4}
let a = [1,2,3,4];
const { cow, animal } = kingdom;
a = a.reduce((sum,a)=>{
  sum+a
}, 0);
```

## How about latex?  

$$x^2 + y^2 = 1$$


```terminal
josh@brick ~/repos/aha $ git log -2
<span style="color:olive;">commit 36b0a3af174e204c8d0a7a993ad467cd7be39bca</span>
Author: Ziz <zizsdl@googlemail.com>
Date:   Fri Aug 3 10:29:43 2012 +0200
    Small version changes, make clean, merging, etc.
...
```


## ls from bash terminal:  

```terminal
rakeen@rak:/media/rakeen/27D5D3A03000703E/dev/project/rakeen_site/gatsby_v1.9$ ls -lah
total 1.1M
drwxrwxrwx 1 rakeen rakeen 4.0K Jan  6 14:02 .
drwxrwxrwx 1 rakeen rakeen 4.0K Jan  5 12:48 ..
drwxrwxrwx 1 rakeen rakeen 4.0K Jan  6 14:07 .cache
-rwxrwxrwx 1 rakeen rakeen  164 Jan  4 23:50 gatsby-browser.js
-rwxrwxrwx 1 rakeen rakeen 1.8K Jan  6 13:39 gatsby-config.js
-rwxrwxrwx 1 rakeen rakeen 2.3K Jan  6 14:01 gatsby-node.js
-rwxrwxrwx 1 rakeen rakeen  180 Jan  4 23:50 gatsby-ssr.js
-rwxrwxrwx 1 rakeen rakeen  188 Jan  4 23:50 .gitignore
-rwxrwxrwx 1 rakeen rakeen 1.1K Jan  4 23:50 LICENSE
drwxrwxrwx 1 rakeen rakeen 232K Jan  6 13:43 node_modules
-rwxrwxrwx 1 rakeen rakeen 1.6K Jan  6 13:43 package.json
-rwxrwxrwx 1 rakeen rakeen 524K Jan  6 13:43 package-lock.json
drwxrwxrwx 1 rakeen rakeen    0 Jan  6 14:02 public
-rwxrwxrwx 1 rakeen rakeen  521 Jan  4 23:50 README.md
drwxrwxrwx 1 rakeen rakeen 4.0K Jan  5 01:27 src
-rwxrwxrwx 1 rakeen rakeen   54 Jan  4 23:47 start-container.sh
-rwxrwxrwx 1 rakeen rakeen 1.7K Jan  6 11:24 todo
-rwxrwxrwx 1 rakeen rakeen 282K Jan  4 23:53 yarn.lock

```


## remark-embed-snippet:  

see: https://github.com/gatsbyjs/gatsby/blob/master/packages/gatsby-remark-embed-snippet/README.md  

```markup
# Sample JavaScript

`embed:sample-javascript-file.js`

# Sample HTML

`embed:sample-html-file.html`
```

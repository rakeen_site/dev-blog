---
title: Shadow DOM
---

One common problem of the web is **encapsulation**.  

Previously we only had **iframes** and **SVG** that had built-in mechanism to isolamte code.  

Shadow DOM isn't a new thing. Turns out, most browsers have been sneakily employing a powerful technique to hide their gory implementation details.  


Shadow DOM refers to the ability of the browser to include a subtree of DOM elements into the **rendering** of a document, but not into the main document DOM tree.

Consider a simple slider:  
`<input id="foo" type="range">`  
There’s a slider track and there’s a thumb, which you can slide along the track.

You won't be albe to see the *markup* on inspect view, nor you'll be able to access those from javascript and you can't change the styles using css(atleast not normally).


Another good example is the video tag.



With Shadow DOM, elements can get a new kind of node associated with them. This new kind of node is called a shadow root. An element that has a shadow root associated with it is called a shadow host. The content of a shadow host isn’t rendered; the content of the shadow root is rendered instead.


### References:  
- https://glazkov.com/2011/01/14/what-the-heck-is-shadow-dom/  
- https://www.html5rocks.com/en/tutorials/webcomponents/shadowdom/
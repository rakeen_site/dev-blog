
# Motivations for Testing

An automated test suite with full coverage enables new contributors  
to push code without worry of breaking anything.  


# Pyramid of Testing  

Say a system has 2 modules each having 5 flows.
If we were only testing the entire system together, we would need to 
write 25 tests for full coverage (5x5 flow combinations). A better 
approach is to mix tests from multiple levels: 5 unit tests for 
Module A by itself, 5 more unit tests for Module B by itself and 1–2 
integration tests of the entire system together.

Applying the multi-level approach to the domain of testing front-end 
applications yields the famous pyramid of testing:  

![](https://cdn-images-1.medium.com/max/1600/1*QItI1DL5m8Od9LzPB5tfDw.png)  


E2E Tests:
Testing the entire app from outside. Running on an actual browser or 
mobile device, with real servers, usually on production.  

UI Automation Tests:
Testing major modules of the app from outside. Running on an actual 
browser or mobile device, with mock servers.  

Component Tests: 
Testing the UI and possibly its integration with business logic from 
inside. Usually running on Node, with mock services.  

Unit Tests: Testing business logic without UI, as separate units. 
Running on Node, mocking everything outside the unit. There’s also 
usually a layer of integration tests between units included here.


> The higher you go up the pyramid, the more brittle and flaky the 
tests are.  

They provide more confidence, but are usually more expensive to write 
and maintain. They also run much slower. A good balance is having a 
lot of tests from the lower levels and fewer and fewer tests from the 
higher levels.



# Unit Test for Reducer

The benefit of pure functions is that there are no side effects so no 
mocking is required.  

One thing we need to watch out for is immutability. This is an example 
where redux-testkit helps as it verifies immutability for us.  
If your state object is complex and deeply nested, redux-testkit 
contains other methods for assertions on state deltas or using custom 
expectations.


# Mocking

https://stackoverflow.com/questions/3459287/whats-the-difference-between-a-mock-stub  

They are very similar to Stub but interaction-based rather than 
state-based. This means you don't expect from Mock to return some 
value, but to assume that specific order of method calls are made.  
i.e  http mocking.  


# Integration  

> There’s a lot of ambiguity regarding the term “integration testing” — 
it all depends on what integrates with what. Let’s be clear and define 
exactly what plays a part in our case. Our focus is on integration 
between business logic units in our client. Not integration between 
client and server. Not integration between business logic and UI.



---------------------

Ref: 

https://hackernoon.com/redux-testing-step-by-step-a-simple-methodology-for-testing-business-logic-8901670756ce

Keywords:

Manual Regression Testing -> https://screenster.io/why-is-manual-regression-testing-still-a-happening-in-2017/
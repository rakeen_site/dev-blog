---
title: Noodles with smoked chicken
date: 24-06-2018
tag:
  - food
  - frozen
  - noodles  
---

I was procrastinating as always and came across this IG page which made me  
realize I should be having fruits regularly.

So I hit the store and bought a bunch of stuff except the food!

Made noodles for dinner, added egg and fried smoked chicken(which was frozen).  

The frozen chicken is shitty as always, got rubbery after frying.  
Needed to seperate the noodles sticks before boiling them; some of them got
stuck with each other.  
Also since the noodles are sticky after boiling the dishes also got messy,
which I didn't clean that night.

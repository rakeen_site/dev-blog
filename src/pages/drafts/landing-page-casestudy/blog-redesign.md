Typographical researchers, Bruijn eta al., discovered that people prefer shorter line lengths when reading content online because it appears more organized and easier to understand.

Going one step further, two other researchers, Dyson and Haselgrove, found that people comprehend shorter line lengths better than longer line lengths.  


- To attract visitors, make the first paragraph's width shorter(or put a half width image beside it); and make the rest full width(480px-600px | 100 char-per-line)  

https://socialtriggers.com/perfect-content-width/  



- People skim read while reading on devices(coz its harder). Show them an estimated time to read.  
- One of the supposed easier to read font on web is sans-serif(particularly Verdana. some use Roboto)  
- the bigger the font size the better it is!  
- italics are harder to read. use bold  
- use capitals when necessary, but don't overuse it  
- use whitespace so that your reader can rest from time to time.  

![](http://3qyfbq1ao4kf1b6kyr3cd5sp.wpengine.netdna-cdn.com/wp-content/uploads/2014/12/SuccessfulBlogging-16-rules.png)
http://www.successfulblogging.com/16-rules-of-blog-writing-which-ones-are-you-breaking/  


- use proper canonicalization!  
- use rel=next,prev tag so that the bots can better understand the context

https://developers.google.com/search/docs/data-types/articles#additional-guidelines  


- see how it appears on text only browser(lynx)



Compare friends site ranking with me  i.e tarifazaz.me
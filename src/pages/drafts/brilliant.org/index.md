Two prime numbers sum to 86479. What is their positive difference?

Since the given number is odd, and even+odd=odd
so the 2 prime numbers are 2 and 86477

[link](https://brilliant.org/practice/prime-numbers-level-1-2-challenges/?problem=prime-5&utm_term=find_these_primes-a&utm_medium=email&utm_campaign=nls-ap0sub2-ob&subtopic=integers&utm_source=brilliant&chapter=prime-numbers-2&auto_login_key=ZBqBwuWzoxuUSFcnNMeJlbHDFSwXHLU7uBKT3Xb99m&utm_content=imageLink)
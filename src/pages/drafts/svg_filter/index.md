---
title: SVG Filters
date: 28-09-2018
---


I had to do this task where I needed to place some chart icons, and make them grey
when selected.
Sounds simple, right?

Turns out there's a lot to it.


Some icons were such that it **needs** to have multiple colors inorder for the
user to understand what type of icon it was.

Take this icon of radial bar chart for example:

![./radial.png]()


So, my initial plan for filling out the svg wasn't a good user experience.
i.e.

```css
svg:hover{
  fill: grey
}
```


The designer had 2 sets of icons, where on is the regular one and another one is the greyscale one.  
So, I took the icons and swapped it with the appropriate one when the icon state mutates.


I knew we can do better than this, since using two sets of images is just too costly over the wire.  
So the solution was to filter the image using css.

```css
img {
  filter: grayscale(1);
}
```


But even this wasn't enought, cause some of the browsers didn't [support it](https://caniuse.com/#feat=css-filters).
So, opt in for polyfill in those rare cases!


This is the end result:

![./end-res.png]()

https://www.facebook.com/mafinar/posts/10155285484065774



Language facilitated Metaprogrammability Tiers
Tier I (Most Powerful)
Lisps (CL, Scheme, Clojure etc), Haskell, Elixir, MLs (OCAML, F# etc), Assembly Languages
Tier II (Powerful)
Ruby, Scala, Rust
Tier III (Somewhat Powerful)
Python, PHP, C#, Io, (Type|Coffee|Java)Script
Tier IV (Tsk Tsk Tsk)
Java, Go, C, C++
Tier V (:P)
Assembly Languages




> More commonly, though, you tend to use metaprogramming for types, rather than values.  

http://stackoverflow.com/a/980698/4437655

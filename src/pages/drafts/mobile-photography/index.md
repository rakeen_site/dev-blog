---
title: 			Better Photo in Mobile
date: 			28-July-2018
layout:			post
description: 	Tips for taking better photos in Mobile
tags:
	- photography
	- mobile
	- tips
og_image: https://www.lifewire.com/thmb/Nc7l-vPlcNqpEZrcjHwWNiv-jBA=/2122x1061/filters:fill(auto,1)/GettyImages-529231383-crop-5a29aff9eb4d52003646ed43.jpg
---

# Lighting

Mobile camera have many limitations(Small sensors, fixed, slow lenses, digital zooms).  
So you need good lighting to take good pictures.  

Shoot when there's plenty of soft light. The golden hour is after the sunrise and before the sunset.  

Get the golden hour timing [here](http://www.golden-hour.com/)  


# Macro  

Mobile's small sensor can capture good macro photos.  
Go close to the item, don't use digital zoom.  
You can even lower the ISO to capture the tiny details.  

# ISO

ISO affects the exposure. Lower ISO means less noise, good photo, but it needs good light.  
Higher ISO means more noise, but good shutter speed. So you can use high ISO to shoot moving object,
or in a low light condition.


---
reference: https://www.lightstalking.com/how-to-make-your-smartphone-photos-look-like-they-came-from-a-pro-camera/

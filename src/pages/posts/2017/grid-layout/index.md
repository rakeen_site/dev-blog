---
title: Grid Layout
layout: post
date: 2017-03-19
path: /css-grid-layout/
---

Css Grid Layout is a 2-dimensional system, meaning it can handle both columns and rows, unlike flexbox which is largely a 1-dimensional system.  
Alongside Flexbox and the Box Alignment Module it will become part of a modern layout system for websites and web applications.  

First we used tables, then floats, positioning and inline-block, but all of these methods were essentially hacks and left out a lot of important functionality (vertical centering, for instance). Flexbox helped out, but it's intended for simpler one-dimensional layouts, not complex two-dimensional ones (Flexbox and Grid actually work very well together).  



## Basic Example:  
```html
<div class="wrapper">
  <div class="box a">A</div>
  <div class="box b">B</div>
  <div class="box c">C</div>
  <div class="box d">D</div>
  <div class="box e">E</div>
  <div class="box f">F</div>
</div>
```

```css
.wrapper {
  display: grid; /* or  inline-grid */
  grid-template-columns: 100px 100px 100px;
  grid-gap: 10px; /* grid-gap: 10px 10px  | grid-row-gap: 10px */
  background-color: #fff;
  color: #444;
}
```
[ref](http://gridbyexample.com/examples/example1/)




## Line Based Placement:
```css
box.a{
	grid-column-start: 2;
	grid-column-end: 3;
	grid-row-start: 1;
	grid-row-end: 2;
}
/* short hand */
box.a{
	grid-column: 2 / 3;
    grid-row: 1 / 2;
}
/* even more shortcut */
box.a{
	grid-area: 1 / 2 / 2 / 3; /* row-start/column-start/row-end/column-end */
}
```

If you do not place an item it will be auto placed by `auto-placement-algorithm`
[ref](http://gridbyexample.com/examples/example17/)

You can also mix and match these two. [ref](http://gridbyexample.com/examples/example19/)  

<br>

The default behavior of Grid Auto Flow is to layout the elements by row.
`grid-auto-flow: column;` on the container would make the auto-flow by columns.



## Order:

```css
.box1 {
  order: 3; /* higher order means it will be painted and placed later in the grid */
}
```
[ref](http://gridbyexample.com/examples/example23/)



## Repeat:
```css
grid-template-columns: repeat(4, [col] 100px ) ;
```

You can also use `auto-fill` and `auto-fit` keywords to fit/fill into the container. [ref](http://gridbyexample.com/examples/example20/)



## Implicit Grid:
```css
.container{
	grid-auto-columns: 100px;
}
```


## Named Area:
```css
.container{
	grid-template-areas:
               "....... header  header"
               "sidebar content content";
}
.sidebar{
	grid-area: sidebar;
}
.header{
	grid-area: header;
}
```


## Layering Items:
You can layer items using z-index.
[ref](http://gridbyexample.com/examples/example15/)


## Grid item as a new positioning context:

```css
.grid-item{ relative }
.thing-inside-item { absolute }
```
[ref](http://gridbyexample.com/examples/example16/)



## Nested Grids:

A grid-item can be grid container also. [ref](http://gridbyexample.com/examples/example21/)



## Box alignment align-items:

```
.wrapper {
  display: grid;
  align-items: center;
}
.grid-item{
	align-self: stretch;
}
```
[ref](http://gridbyexample.com/examples/example24/)

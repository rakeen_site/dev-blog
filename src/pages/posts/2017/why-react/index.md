---
title: Why was react built?
date: 2017-03-26T15:30:00+06:00
author: Shadman Rakeen
layout: post
path: /why-react/

meta_description: In this post I tried to went back before react and see what problems that we had that cried for yet another front-end library like react.
og_image: https://s3.amazonaws.com/fullstackfeed/images/react-2.jpg
tags:
  - react
  - web
  - basic
  - javascript
  - reactive programming
---


<!--
On 2015-2016-ish year there was this javascript boom and like many of you those things lit up my happy area of the brain.  
So I made a plan that I'll be using `angular 2` for my academic Web Technology course. `angular 2` was in **alpha** when I started
and I DO NOT recommend building a serious project using alpha product(unless you have plenty of time and energy).
-->


![](https://s3.amazonaws.com/fullstackfeed/images/react-2.jpg)<!-- {width=100%} -->

# Prologue

I did a [hackathon](https://www.facebook.com/events/260278807747280/) <span title="March 2017">this month</span> where my team planned to build a crud-like web app.
We decided to go with react(along with other stuff under react umbrella) and golang. As easy the project might sound we took one of the complex(read hipster) technologies for building the <abbr title="Minimal Viable Product">MVP</abbr>. Being ambitious, stubborn and stupid we couldn't present the MVP.  

<br>

Later I though about it and tried to reason about using react. I thought to myself hey may be it's because of the reusable components; but wait 🤔
`web components` also does that. Maybe it's because of the data handling it does ... or maybe their [*reconciliation algorithm*](https://facebook.github.io/react/docs/reconciliation.html) is super fast!  

This post focuses on the problems that react tries to solve.  


# What is React

> React is a library for building composable user interfaces.

The creator of react [Jordan Walke](https://en.wikipedia.org/wiki/React_(JavaScript_library)#History)  was influenced by XHP(an HTML component framework for PHP) and probably conceived the idea of polluting javascript with html markup; as they call it `JSX`.



# Problems react tries to solve

### Avoid **templates**  
By baking an understanding of markup and content into JavaScript, there's no manual string concatenation and therefore less surface area for XSS vulnerabilities.  

### Reactive Updates  

> React really shines when your data changes over time.  

In a traditional JavaScript application, you need to look at what data changed and imperatively make changes to the DOM to keep it up-to-date.  
React on the other hand `renders` your markup efficiently by using their diff-ing algorithm(a.k.a reconciliation) which only updates the necessary part of the actual DOM.  

Because this re-render is so fast (around 1ms for TodoMVC), the developer doesn't need to explicitly specify **data bindings**.  


<br>

 **Reactive programming**: this is one of those phrases that threatens to become meaningless with overuse, but it's a useful concept. Put simply, in a reactive system where the value of b depends on the value of `a`, if `a` changes then `b` will also change. Applied to user interfaces, that means that when your application state changes, your view also changes. With traditional MVC libraries you typically have to implement all your render logic manually and wire it up with a web of publish/subscribe events; with React you're spared that (tedious, error-prone, hard-to-optimise) step when building your apps.


🔥 ⚡ They are building [**react fiber**](https://github.com/acdlite/react-fiber-architecture) which is a reimplementation of their core algorithm(including reconciliation algorithm).  



# Epilogue

My takeout from this 'research'(!) is if you're building a data extensive <abbr title="Single Page Application">SPA</abbr> where you need to react to user interaction; using react would give you some major advantage over traditional tech stack.  




<br><br><hr>

References:  
- https://thenewstack.io/javascripts-history-and-how-it-led-to-reactjs/
- https://facebook.github.io/react/blog/2013/06/05/why-react.html  
- https://facebook.github.io/react/docs/reconciliation.html  
- http://blog.ractivejs.org/posts/whats-the-difference-between-react-and-ractive/  

---
title: Semantic Image
layout: post
date: 2017-03-19T22:05:00+06:00
path: /semantic-image/
---


# Figure

The HTML `<figure>` element represents self-contained content, frequently with a caption (`<figcaption>`), and is typically referenced as a single unit.  

<br>

**Usage Notes:**
- Usually a `<figure>` is an **image**, **tables**,**charts**, **illustration**, **diagram**, **code snippet**, **videos**, **audio clips**, **advertisement** etc., that is *referenced in the main flow* of a document, but that can be *moved* to another part of the document or to an appendix without affecting the main flow.  
- Being a sectioning root, the outline of the content of the `<figure>` element is excluded from the main outline of the document.  
- A caption can be associated with the `<figure>` element by inserting a `<figcaption>` inside it (as the first or the last child).  

<br>

The figure element represents a self-contained unit of content. This means that if you were to move the element either further down a document, or to the end of the document, it would not affect the document’s meaning.
> Therefore, we also need to remember that not every image is a figure.  



```html
<figure>
  <img src="tea.jpg" alt="Tea cup with steam and pen on bed">
  <figcaption>Journaling with Tea</figcaption>
</figure>

<figure>
  <figcaption><cite
      >Edsger Dijkstra :-</cite></figcaption>
  <p>"If debugging is the process of removing software bugs,
  <br />
  then programming must be the process of putting them in"</p>
</figure>
```


1. You can put multiple images inside figure  
2. You can nest figure inside another figure(if that makes sense)!

```html
<figure role="group">
  <figcaption>Dog breeds</figcaption>
  <figure>
    <img src="dog1.jpg" alt="Maltese Terrier">
    <figcaption>Adorable Maltese Terrier</figcaption>
  </figure>
  <figure>
    <img src="dog2.jpg" alt="Black Labrador">
    <figcaption>Cute black labrador</figcaption>
  </figure>
</figure>
```

[**Sectioning Root:**](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Using_HTML_sections_and_outlines#Sectioning_root) A sectioning root is an HTML element that can have its own outline, but the sections and headings inside it do not contribute to the outline of its ancestor.


[right way to use figure | georgie luhur - sitepoint](https://www.sitepoint.com/quick-tip-the-right-way-to-use-figure-and-figcaption-elements/)
[MDN - figure](https://developer.mozilla.org/en/docs/Web/HTML/Element/figure)


# Picture

The HTML `<picture>` element is a container used to specify multiple `<source>` elements for a specific `<img>` contained in it. The browser will choose the most suitable source according to the current layout of the page (the constraints of the box the image will appear in) and the device it will be displayed on (e.g. a normal or hiDPI device.)



```html
<picture>
  <source
    media="(min-width: 650px)"
    srcset="images/kitten-stretching.png,
            images/kitten-stretching@1.5x.png 1.5x,  
            images/kitten-stretching@2x.png 2x">
  <source
    media="(min-width: 465px)"
    srcset="images/kitten-sitting.png,
            images/kitten-sitting@1.5x.png 1.5x
            images/kitten-sitting@2x.png 2x">
  <img
    src="images/kitten-curled.png"
    srcset="images/kitten-curled@1.5x.png 1.5x,
            images/kitten-curled@2x.png 2x"
    alt="a cute kitten">
</picture>
```

[MDN - picture](https://developer.mozilla.org/en/docs/Web/HTML/Element/picture)



# Picture inside Figure?

[Totaly fine and recommended!](http://stackoverflow.com/a/23941403/4437655)

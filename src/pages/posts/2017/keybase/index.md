---
title: keybase.io
layout: post
date: 2017-03-05
path: /keybase/
---

![keybase.io](https://longren.io/wp-content/uploads/2015/04/keybase-logo.jpg)<!-- {width=100%} -->  


Today the world(well most if it!) depends on centralized+hierarchical [PKI(public key infrastructure)](https://en.wikipedia.org/wiki/Public_key_infrastructure) for secured-encrypted communication.  
In this system some **Certification Authority**(CA) issues certificates to a lot of sub-entities: the CA signs certificates to guarantee the link between an identity and the public key owned by that entity.

Pretty Good Privacy otherwise known as [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) is an encryption program. The first version of it uses what is known as the [Web of Trust(WOT)](https://en.wikipedia.org/wiki/Web_of_trust), where every user is a CA as opposed to PKI.

In mathematical languages, a hierarchical PKI naturally implies an acyclic certification graph, usually a tree: trust percolates down the tree from a single root CA, or a small set of root CA. Whereas the WoT is a super-connected generic graph with cycles and many paths between any two points.

> Politically, the hierarchical PKI is a military-inspired structure, with a central chain of command; while the WoT is an anarchist hippy utopia in which trust emerges semi-magically from the assembled people (or the mob, from another point of view).  
~ http://security.stackexchange.com/a/61366

Although WOT has [advantages](https://andrewgdotcom.wordpress.com/2014/11/13/wot-ca/) over PKI, it hasn't been universally used.  

---

💥 [keybase](https://keybase.io/) uses your social accounts(along with other stuff) to bulletproof key exchange and thus secures your identity.  
- You first create a private-public key pair and verify your accounts.  
- Anyone can now find your public key(and verify them) from any of the sites that you have verified
- If any of your account is compromised, you can then revoke it from keybase.  

![Bidirectional Graph of WOT](./graph.png "Bidirectional Graph of web of trust")  

The graph of this model is bidirectional. It is practically impossible(well ...) to compromise one's identity, as it would require one to compromise every single node and that too without anyone noticing it.  

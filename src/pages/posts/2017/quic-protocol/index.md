---
title: QUIC Protocol
layout: post
date: 2017-02-09
path: /quic-protocol/
author: rakeen
tag:
  - web
  - protocol
  - quic
  - google
---

![](http://1.bp.blogspot.com/-E-Ota2OWqA4/VTOFBv8L8SI/AAAAAAAAioA/xnxMA19XdgI/s1600/Quick-UDP-Internet-Connections-quic.jpg)<!-- {width=100%} -->


# QUIC  
Quick UDP Internet Connections is an  transport layer network protocol designed by **Jim Roskind** at Google, initially implemented in 2012.  

- Dramatically reduced connection establishment time
- Improved congestion control
- Multiplexing without head of line blocking
- Forward error correction
- Connection migration

QUIC is a new transport which reduces latency compared to that of TCP. On the surface, QUIC is very similar to TCP+TLS+HTTP/2 implemented on UDP. Because TCP is implemented in **operating system kernels**, and middlebox firmware, making significant changes to TCP is next to impossible. However, since QUIC is built on top of UDP, it suffers from no such limitations.

QUIC supports a set of *multiplexed* connections between two endpoints over UDP, and was designed to provide **security protection** equivalent to TLS/SSL, along with reduced connection and transport latency, and **bandwidth estimation** in each direction to avoid *congestion*.
QUIC's **main goal** is to improve perceived performance of connection-oriented web applications that are currently using TCP.

Google is [already](https://www.ietf.org/proceedings/96/slides/slides-96-quic-3.pdf) using QUIC on its major sites.  

![](./quic-devtools.png)
<!-- insert devtool sample protocol -->


## Further Reading:
- https://ma.ttias.be/googles-quic-protocol-moving-web-tcp-udp/
- [QUIC FAQ for geeks](https://docs.google.com/document/d/1lmL9EF6qKrk7gbazY8bIdvq3Pno2Xj_l_YShP40GLQE/edit)  

#### Reference
[Chromium](https://www.chromium.org/quic)  
[Wikipedia](https://en.wikipedia.org/wiki/QUIC)
---
title: Happiness Factor in Work
author: rakeen
date: 2018-12-12T22:34:00+06:00
layout: post
path: /happiness-in-process/
og_image: https://img.youtube.com/vi/lU5xIZ4bemI/hqdefault.jpg
meta_description: Note on happiness of doing work by standup comedian Aakash Mehta on Josh Talks.
---

`youtube:https://www.youtube.com/watch?v=lU5xIZ4bemI`


- comedy is little more work than it seems.
- and it's hard to show the effort because it's thinking joke.
you do a joke it doesn't work, you do that again it doesn't work.
by the fifth time you think you should give up standup comedy.
- in standup you are the only writer, director and scripter. so you 
can't blame anyone. And if the joke isn't funny, it isn't funny!
- every joke you see on youtube is tried 500 times.


> the only way to realy be happy is to fall in love with the process of doing something


> what is the best book you have written?
it's the next one

> the race is really long and it's very lonely
and the only person you are really racing with is yourself



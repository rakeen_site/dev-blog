---
title: Flow Science
author: rakeen
date: 2018-06-26T11:00:00+06:00
layout: post
path: /flow-science/
og_image: https://img.youtube.com/vi/bshSbNFXLww/hqdefault.jpg
meta_description: Note on the google talk of maximizing human potential.
---

`youtube:https://www.youtube.com/watch?reload=9&v=bshSbNFXLww`

![flow science note](flow-science-note.jpg)

---
title: Building just another management system
layout: post
date: 2015-04-27
author: Shadman Rakeen
path: /hrms/

meta_description: I built yet another 'management system' for my database course. Here I tried to note down my mistakes, architectural decisions and development journey.  
og_image: https://raw.githubusercontent.com/rakeen/HRMS/master/screenshots/home.png

tags:
  - DB
  - databse
  - laravel
  - project
  - cse311L
---


> *this blog was meant to be a reminder of what to avoid in the next project!*
> and to note down some facts about **Laravel**

<br><br>

# Choosing The Topic:
I had a plan of imitating moon vaiya's [project](http://github.com/tmoon/TarikDB). But later on pinku and imtiaz came along and ultimately *we* had to do a project given by our instructor!

# The Project:
The project is about **human resource management system** *(HRMS)* . Meaning, we had to build a system which would automate the tasks of managing the *human resource*!

## Features:
We had to go through several drafts and make endless edits, keeping in mind that we have limited time.


+ Home Page of a sample Software Firm
+ Apply for Job
+ Recruiting Employees
+ Dashboard for Users
+ Profile Page
+ Managing Attendance
+ Edit Salary
+ Posintg Notice


## Designing The Database:

I didn't have much knowledge about the DB construction techniques(eg. maintaining certain properties, normalization) at the time of starting this project. So, the DB just grew without any 'constructive' planning with the porject itself. Later on pinku pointed out there's some flaw in it(i.e the DB is not well defined).  
Next time when you design a DB take your time, list the attributes and relations. Make ER diagram, modify it, normalize it and then make the actual tabels.


# Authentication & Dashboard:

I looked on some tutorials and made a simple authentication system in no time. Turns out laravel have some outstanding feautres which eases the process. *XXS, CSRF, SQLi* you name it!
However a challenge was to change the default authentication procedure(i.e authenticating with userID+pass which is on different table!). I'll come back to this later.

# Migration and Sleepless Nights:

![](http://vegibit.com/wp-content/uploads/2015/03/generate-migrations-from-existing-database.png)

We obviously didn't finalize our database and then started to code. Instead what we did is made a barebone DB and started to modify it. Migration keeps track of these changes in the database. But turns out there's a certain procedure in which you're to rollback in a specific state. In some cases you won't be able to revert back!
Another important fact to look for is, you can't rollback into *specific migration*.

For example:

	php artisan migrate:make create_user
	php artisan migrate:make add_userID
	php artisan migrate:make add_userPassword
	php artisan migrate:make add_userEmail

Suppose you ran all the migrations above after creating it. Now you can obviously rollback the **add_userEmail** migration. But, you can't rollback **userID** right now!

What I did instead is create another migration and deleted those parts which I didn't need.

# Baybalde and Forms:

![](http://i.ebayimg.com/00/s/MzkwWDM5Mw==/z/pEAAAMXQVT9SsRcf/$_35.JPG)

Turns out you can't use raw html forms in laravel. You have to use blade's form, and it doesn't support all the input types. So, I tried to make some custom input types using bootstrap plugin. I couldn't finish the thing but hey atleast the custom element showed up!


# The Saga of Gittu:

I never had actually used github. So this problem was **immenent** I guess.   

![](http://s2.quickmeme.com/img/19/19e90aca81517dc837250c7b54ddc5240d03396958d41936500d5585d3baa5cc.jpg)

**gitignore:** Here you can actually tell git not to add files. Althouht you can override this using **force** flag(which I eventually did! :P ).  
<br>
**push-pull sync**: your local version have to be synced with the remote version. if not you've to pull it.  
<br>
**commit and push**: before pushing you've to commit something!  
<br>
**commit history**: don't ever delete any commit. This will destroy the version controlling thing!  
<br>


# Templating the shit out-off it:

![](https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/03/1427031865laravelblade.png)

You can't yield nested templates in blade. So, I though how to resolve this problem. Turns out there isn't much you can do; except for bypassing the situation. One option I found is to take advantage of blade's controll structure and use it to achieve the templating.

# File Uploading:
**->getFileName()**

# The Magic of Eloquent ORM:
This thing is so cool I forgot abou the basic sql query and relational algebra that we were taught in class. At a time I almost designed the whole database without any relation in it(cause all the relations were defined in the controller!).   
There're few things to look for.

**Performance:** be very carefull about using any kind of ORM. slight change of the query may cause havoc! Also, you need tons of experience to write effecient query even in ORM.  
<br>
**Looping the Result:** use get() method to turn the returned obeject into an array, so that you can do a loop over it.
**Get the first Elem:** sometimes you know the result will be only one element, but the ORM will always return an array(or object?). So you've to use the **first()** method to catch the first result.

# Dev and Prod:
Configure the development and production version of your project. Trust me, it'll save a lot of time when you'll try to deploy your project online.


## Things to Know:  

- Mailing
- Queuing
- Sub-domain routing

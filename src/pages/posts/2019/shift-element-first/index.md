---
title: Extending 3rd Party React Component
author: rakeen
date: 2019-02-07T12:16:00+06:00
layout: post
path: /extending-rect-component/
og_image: https://cdn-images-1.medium.com/max/1600/1*V4anjW4im3wZVHJnbb6YIA.png
meta_description: Stuffs I tried while implementing a new design.
---


The task was to make the input element appear before the tags and all the tags should be in the next line.  


![Objective](./before-after.png)  


Pretty easy task, you literally just make the `tagInput` element appear first inside the container.

![The markup](./react-tag_markup.png)  


But the catch was we were using a 3rd party react component(`react-tags`) which controlled the generation of the tags element internally.  

I didn't want to fork the `react-tags` and modify it.  
I started trying to achieve this using only css.  

Tried using `float` but that made the tags be in the same line as the input element.  

![CSS Float](./float-left.png)  


Then I tried to bring the tags and input elemen inside a css grid, which not surprisingly failed miserably. 
I wanted the tags to grow along with the text content.  

![CSS Grid](./tag-grid.png)  


After many trial and error I got frustated and figured maybe grid isn't the solution here. Later I opted for manipulating the DOM itself with jquery and be done with it! 😛  

But simply manipulating the DOM on the container component wasn't enough either 😒!  
Because the tag DOM gets generated after the rendering of the react-tags component.  
So I had to somehow manipulate the DOM after the rendering of the react-tag component. But I can't modify the definition of the react-tag component (since it's a 3rd party component).  

So what I ended up doing is create another Higher Order Component(HoC), do the DOM manipulation after that component has been mounted, and put the 3rd party react-tag component inside that HoC.  

![React HoC](./hoc.png)
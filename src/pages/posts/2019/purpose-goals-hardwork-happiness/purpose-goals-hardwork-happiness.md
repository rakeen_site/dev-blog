---
title: Purpose, goals, hard work and never ending pursuit of happiness
date: 2019-12-22T16:00:00+06:00
author: rakeen
layout: post
path: /purpose-goals-hardwork-happiness/
og_image: https://miro.medium.com/max/3600/0*7Y1WCU5t7R3PV7De
meta_description: Excerpts from a blog post about purpose, goals and hard work.
---


Excerpts from [Hard Work, Life Goals & The Never Ending Pursuit of Happiness by Faris Sweis](https://medium.com/@farissweis/hard-work-life-goals-the-never-ending-pursuit-of-happiness-7ea868a69ef9?)

<br>

Examining your beliefs and mental model help to check if they are still true 
in today's current environment and conditions.
If they are you continue to operate under the same model with greater and deeper 
clarity.
If not, you can go digging for more answers.

One other shade of that questioning that I found interesting was not only asking whether the model holds true, but also whether they have helped or hurt or made no different in some cases — a truly harder question to answer.

---

- hard work is personal  
- if you reach your goals, no one cares about the hard work(except for the addicted work&stress junkie side of yours)  
- goals have a Purpose  
- purpose is very personal  
    - Purpose gives you a glimse into people’s worldview
    - It is hard to define purpose
- Why have a purpose?
    - bring me happiness, self-glorifying  
- happiness is in the work of **today**
    - don't tie your wellbeing in these outcomes  
    - even if you achieve those outcomes, the feeling of satisfaction will be transient  



<!--
link archive: https://web.archive.org/web/20191222100444/https://medium.com/@farissweis/hard-work-life-goals-the-never-ending-pursuit-of-happiness-7ea868a69ef9
-->

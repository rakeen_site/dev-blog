---
title: Passing props to every children
author: rakeen
date: 2019-03-07T03:15:00+06:00
layout: post
path: /passing-props-to-children/
og_image: https://www.robinwieruch.de/img/posts/react-pass-props-to-component/banner_1024.jpg
meta_description: How to pass react props into every children
---


## The problem  

You have a ⚛ higher order component(HoC) like this:  

![](./hoc.png)  

You need to pass the prop `color` inside the children of the `Parent` component.  


## cloneElement to the rescue  

One straight forward solution as described [here](https://stackoverflow.com/questions/32370994/how-to-pass-props-to-this-props-children) is to clone the children and inject our desired props!  

![](./injected-props.png)  


There was another approach using [context](https://stackoverflow.com/a/39401252/4437655) with some caveats, but the bottom line is context and props are just not the same thing.  



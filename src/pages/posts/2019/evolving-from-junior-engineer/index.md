---
title: What junior engineers don't know
author: rakeen
layout: post
date: 2019-03-25T09:17:00+06:00
path: /evolving-from-junior-engineer/
og_image: http://i3.ytimg.com/vi/NMuL6xOY1Ro/maxresdefault.jpg
meta_description: Notes on how to evolve form junior engineer
---


`youtube:https://youtu.be/NMuL6xOY1Ro`



- engineering is not just about being correct. there's more to it.  
- supreme art of war is to subdue your enemies without fighting - tzun tzu in art of war  
supreme excellence consists of breaking enemy's resistance without fighting.  
- No one cares about your code. Only you read your actualy code. Save everyone's time and talk what people want to hear.   
- stop fixing all the bugs you are assigned!  
Stop whining about how you are busy doing all these bugs.  
bug will come and go. You need to take ownership and make an impact.  
- take feedback.  
if you constantly challenge your mentor, you won't get feedback.
instead ask questions politely.  








---
title: Simple Adulthood
author: rakeen
layout: post
date: 2019-01-05T12:00:00+06:00
path: /simple-adulthood/
og_image: http://i3.ytimg.com/vi/ZOgvWIulxho/maxresdefault.jpg
meta_description: notes on simple adulthood
---


`youtube:https://www.youtube.com/watch?v=ZOgvWIulxho`




Charlife might even be the personification of that simpletrashcan,
simple, elegant, classy, stainless steel.

And what are you!  
Is that even stain on your trashcan?  

Was that stain always there!  
You're life is a mess.  
You're food expired.  
Your style expired.  


Learn the basics. The squats and dumbells,
you can't rely solely on your metabolism.  


Now that you are doing all these things, your time becomes more valuable.  
You start sticking to your schedule.  
But don't worry, you can sometimes set aside time for doing nothing!  


Becoming adult isn't about eating kale or making a lot of money or buying new clothes.  
Becoming adult is about taking control of your life, and living up to your full potential.  








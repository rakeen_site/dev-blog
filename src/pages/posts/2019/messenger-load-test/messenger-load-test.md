---
title: How facebooks software engineers prepare messenger for new years eve
date: 2019-01-31T12:00:00+06:00
author: rakeen
layout: post
path: /messenger-load-test/
og_image: https://mondrian.mashable.com/uploads%252Fcard%252Fimage%252F332966%252Fcb42bfe1-5156-4d6a-8768-f52bd9ec6dd2.png%252F950x534__filters%253Aquality%252890%2529.png?signature=qCo9FPz47T5rJLN320I9GeDQW7A=&source=https%3A%2F%2Fblueprint-api-production.s3.amazonaws.com
meta_description: How facebook team deal with deterministic flux of traffic.
---


# Direct Traffic

Direct traffic from one data center to another!


# Graceful degradation

Every message goes through a queue as part of a service called `Iris`.  
There, messages are assigned a timeout—a period of time after which, that message will drop out of the queue to make room for new messages.  
During a high-volume event, this allows the team to quickly discard certain types of messages, 
such as **read receipts**, to focus its resources on delivering ones that users have composed.


“We set up our systems so that if it comes to that, they start shedding the lowest-priority traffic,” says Ahdout. “So if it came to it, Iris would rather deliver a message and drop the read receipt, rather than drop the message and deliver the read receipt.”


Georgiou says the group can also sacrifice the accuracy of the green dot displayed in the Messenger app that indicates a friend is currently online. Slowing the frequency at which the dot is updated can relieve network congestion. Or, the team could instruct the system to temporarily delay certain functions—such as deleting information about old messages—for a few hours to free up CPUs that would ordinarily perform that task, in order to process more messages in the moment.

	


https://spectrum.ieee.org/tech-talk/computing/software/how-facebooks-software-engineers-prepare-messenger-for-new-years-eve

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container } from 'react-responsive-grid';
import Link from 'gatsby-link';
import Helmet from 'react-helmet';
import {formatDateWithOutTime} from '../utils';
import { rhythm } from '../utils/typography';

export default class Index extends Component {
  generateBlogMeta(){
    const tmpl = (title,url,og_img,date) => `{
      "@type": "BlogPosting",
      "mainEntityOfPage": "https://blog.rakeen.gq${url}",
      "headline": "${title}",
      "author": {
        "@type": "Person",
        "name": "Shadman Rakeen"
      },
      "datePublished": "${date}",
      "image": {
        "@type": "imageObject",
        "url": "${og_img}"
      },
      "publisher": {
        "@type": "Person",
        "@id":"https://rakeen.gq/#rakeen"
      }
    }`;

    let ret = "";
    const posts = this.props.data.posts.edges;
    posts.forEach((post,i)=>{
      const {title, path, og_image, date} = post.node.frontmatter;
      ret+=tmpl(title, path, og_image, date);
      if(i<posts.length-1) ret+=",";
    });
    return ret;
  }
  displayPosts(){
    const posts = this.props.data.posts.edges;
    let pageLinks = [];
    posts.forEach((post)=>{
      pageLinks.push(
        <li
          key={post.node.frontmatter.path}
          style={{
            marginBottom: rhythm(3/4)
          }}
        >
          <Link
            to={post.node.frontmatter.path}
            className="page-link"
          >{post.node.frontmatter.title}</Link>
          <div style={{
            fontSize: '12px',
            opacity: '0.9'
            // color: 'lightgrey',
          }}>
            { formatDateWithOutTime(post.node.frontmatter.date)}
          </div>
        </li>,
      );
    });
    return pageLinks;
  }
  render(){
    /*
      Blog can have a list of blogPosts
      https://jsonld-examples.com/schema.org/code/blog-markup.php

      Alternatively you can do ItemList
      https://webmasters.stackexchange.com/a/99106
      https://developers.google.com/search/docs/guides/mark-up-listings
    */
    /*
      WebSite(WebPage, Article), Blog(blogPost,TechArticle)
      http://schema.org/CreativeWork
    */
    /*
      On @id and IRI:
      https://stackoverflow.com/questions/34761970/schema-org-json-ld-reference/34776122#34776122
      https://stackoverflow.com/questions/32177780/json-ld-define-a-person-for-easy-reuse-as-values-on-different-keys-for-webpa
      https://stackoverflow.com/questions/41071235/id-vs-url-for-linking-json-ld-nodes
    */
    /*
      Google only supports Articles from CreativeWork(no blogPosts) as of Jun 3, 2017
      https://developers.google.com/search/docs/guides/mark-up-content?visit_id=1-636320470876743026-4030861293&hl=en&rd=1
    */
    /*
      Whilst schema.org does support the type Person for the publisher property of Article, Google does not.
      Google will simply ignore the Publisher field in non-AMP pages.
      https://stackoverflow.com/questions/40565885/the-attribute-publisher-itemtype-has-an-invalid-value#comment72356556_40586036
      https://developers.google.com/search/docs/data-types/articles

      If you really need to add Publisher then use the blog name as the organization's name.
      ref: https://clicknathan.com/web-design/fix-a-value-for-the-logo-field-is-required-via-googles-rich-snippets-tool/
    */

    const siteTitle = this.props.data.site.siteMetadata.title;
    const posts = this.props.data.posts.edges;
    const json_ld = `{
      "@context": "http://schema.org",
      "@type": "Blog",
      "name": "dev blog | rakeen",
      "url": "https://blog.rakeen.gq",
      "isPartOf": {
        "@id": "https://rakeen.gq/#site",
        "@type": "WebSite"
      },
      "description": "dev blog by rakeen",
      "sameAs": [
        "http://blog.rakeen.gq/"
      ],
      "author": {
        "@id":"https://rakeen.gq/#rakeen",
        "name": "Shadman Rakeen"
      },
      "publisher": {
        "@id":"https://rakeen.gq/#rakeen"
      },
      "blogPosts": [
        ${this.generateBlogMeta()}
      ]
    }`;

    return (
      <div>
        <Helmet
          title={siteTitle}
          link={[
            { "rel": "canonical", "href": "https://blog.rakeen.gq/"}
          ]}
          meta={[
            { "name": "author", content: "Shadman Rakeen" },
            {"name": "description", "content": "dev blog | rakeen"},
            {"name": "keywords", "content": "software, web"},
          ]}
          script={[
            { "type": "application/ld+json", "innerHTML": json_ld }
          ]}
        />
        <Container style={{display: 'flex', justifyContent: 'center', maxWidth: '640px'}}>
          <ul style={{
            listStyle: 'none',
            margin: '0'
          }}>
            {this.displayPosts()}
          </ul>
        </Container>
      </div>
    );
  }
}

Index.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string,
          frontmatter: PropTypes.shape({
            date: PropTypes.string.isRequired,
            path: PropTypes.string.isRequired,
            tags: PropTypes.string,
            title: PropTypes.string.isRequired,
          })
        })
      ),
    }),
  }).isRequired,
};

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    posts: allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] }
    ) {
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            path
            tags
            meta_description
            og_image
          }
        }
      }
    }
  }
`;

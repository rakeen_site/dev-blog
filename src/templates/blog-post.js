import React, { Component } from "react";
import { formatDate, formatDateWithOutTime } from '../utils';
import Helmet from 'react-helmet';
import { rhythm } from '../utils/typography';

import striptags from 'striptags';
import readingTime from 'reading-time';

import '../css/prism-onedark.css';
import '../css/prism-hightlight.css';
import 'katex/dist/katex.min.css';

const estimatedReadingTime = (markup) => {
  const {text,words} = readingTime(striptags(markup), {
    wordsPerMinute: 200
  });
  return {
    readingTime: text,
    wordCount: words
  };
}

const json_ld = (title, path, meta_description, date, og_image) => {
  return `{
    "@context": "http://schema.org",
    "@type": "BlogPosting",
    "name": "${title}",
    "headline": "${title}",
    "url": "https://blog.rakeen.gq${path}",
    "description": "${meta_description}",
    "author": {
      "@id":"https://rakeen.gq/#rakeen",
      "@type": "Person",
      "name": "Shadman Rakeen"
    },
    "publisher": {
      "@type": "Person",
      "@id":"https://rakeen.gq/#rakeen"
    },
    "datePublished": "${new Date(date).toISOString()}",
    "image": {
      "@type": "imageObject",
      "url": "${og_image}"
    }
  }`;
};

class BlogPostTemplate extends Component {
  render() {
    const {
      date,
      title,
      tags,
      path,
      author="Shadman Rakeen",
      meta_description,
      og_image
    } = this.props.data.markdownRemark.frontmatter;
    const { siteTitle } = this.props.data.site.siteMetadata;
    const { readingTime, wordCount } = estimatedReadingTime(this.props.data.markdownRemark.html);
    return (
      <div className="markdown">
        <Helmet
          title={`${title} | ${siteTitle}`}
          link={[
            { "rel": "canonical", "href": "https://blog.rakeen.gq"+`${path}` }
          ]}
          // type check post.meta
          meta={[
            { "name": "author", content: `${author}` },

            { "property": "og:url", content: "https://blog.rakeen.gq"+`${path}` },
            { "property": "og:title", content: `${title}` },
            { "property": "og:type", content: "article" },
            { "property": "og:article:author", content: "https://facebook.com/s.rakeen" },
            { "property": "og:description", "content": `${meta_description}` },
            { "property": "og:image", content: `${og_image}` },

            { "name": "description", "content": `${meta_description}` },
            { "name": "keywords", "content": `${tags}` },
          ]}
          script={[
            { "type": "application/ld+json", "innerHTML": json_ld(title, path, meta_description, date, og_image) }
          ]}
        />
        <header style = {{
          textAlign: 'center',
          marginBottom: '10px'
        }}>
          <h1>{title}</h1>
          <em
            style={{
              display: 'block',
              marginBottom: rhythm(2),
              fontSize: '0.8em',
              opacity: '0.9'
            }}
          >
            Posted on { formatDate(date) } | {wordCount} words &middot; {readingTime}
          </em>
        </header>
        <article>
          <div dangerouslySetInnerHTML={{ __html: this.props.data.markdownRemark.html }} />
        </article>
      </div>
    );
  }
}

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      timeToRead
      frontmatter {
        date
        path
        title
        tags
        og_image
        meta_description
      }
    }
    site {
      siteMetadata {
        siteTitle: title
      }
    }
  }
`;

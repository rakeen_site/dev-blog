import moment from 'moment';

// src: https://github.com/iamdustan/iamdustan.github.io/blob/gatsby/utils/index.js
export const pathToDate = (path) => {
  const dateParts = path.replace(/^\//, '').slice(0, 10)
  return moment(dateParts).format('MMMM D, YYYY')
}


export const formatDate = (date) => {
  return moment(date).format('dddd · MMMM DD YYYY · hh:mm A')
}
export const formatDateWithOutTime = (date) => {
  return moment(date).format('dddd · MMMM Do, YYYY')
}

// src: https://gist.github.com/thevangelist/8ff91bac947018c9f3bfaad6487fa149#gistcomment-2063326
export const kebabCase = (string) =>
  string.replace(/([a-z])([A-Z])/g, "$1-$2")
   .replace(/\s+/g, '-')
   .toLowerCase();

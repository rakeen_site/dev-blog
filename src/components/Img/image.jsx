/*
Create an Img component for ease of use.
  Should follow proper a11y practice
  Should lazy load
  Should instantly load a small version of the image stretched (add automation for this task)
  Should load those images first which are currently visible in the user's view; to avoid FOUC. Can use intersection observer
  Add a spinner while you're pulling the actual image
*/

import React, { Component } from 'react';

export default class Img extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <figure>
        <img src={this.props.src} alt="" aria-label="" title="" />
        <figcaption></figcaption>
      </figure>
    );
  }
}

Img.propTypes = {
  src: React.PropTypes.string,
  title: React.PropTypes.string,
  alt: React.PropTypes.string,
  ariaLabel: React.PropTypes.string
};

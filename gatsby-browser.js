/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

 // You can delete this file if you're not using it

const GA = require('./gatsby-config').siteMetadata.GA;
// import ReactGA from 'react-ga';
//
// ReactGA.initialize(GA);
//
// const logPageView = () => {
// 		ReactGA.set({ page: window.location.pathname });
// 		ReactGA.pageview(window.location.pathname);
// 		ReactGA.timing({
// 		  category: 'perf.now',
// 		  variable: 'init',
// 		  value: Math.round(window.performance.now()), // in milliseconds
// 		  label: 'perf.now'
// 		});
// }
//
export const onRouteUpdate = (state,page,pages) => {
		if(window.ga){
      if(window.performance){
        window.ga( 'send', {
          hitType: 'timing',
          timingCategory: 'perf.now',
          timingVar: 'perf.now',
          timingValue: Math.round(window.performance.now()), // in milliseconds
        });
      }
      else{
        window.ga('send', {
          hitType: 'event',
          eventCategory: 'browser',
          eventAction: 'feature',
          eventLabel: 'No window.perf'
        });
      }
    }
}

[Gatsby Latest: ![npm version](https://badge.fury.io/js/gatsby.svg)](https://badge.fury.io/js/gatsby)
<!-- fetch('https://api.github.com/repos/zeit/hyper/releases/latest').then(r=>r.json()).then(d=>console.log(d.tag_name)) -->

# gatsby-starter-default
The default Gatsby starter

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/)

Install this starter (assuming Gatsby is installed) by running from your CLI:
```
gatsby new gatsby-example-site
```

## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-default)

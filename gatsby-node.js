/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require("path");


// src: https://gist.github.com/thevangelist/8ff91bac947018c9f3bfaad6487fa149#gistcomment-2063326
const kebabCase = (string) =>
  string.replace(/([a-z])([A-Z])/g, "$1-$2")
   .replace(/\s+/g, '-')
   .toLowerCase();

exports.onCreateNode = ({ node, boundActionCreators, getNode }) => {

  const { createNodeField } = boundActionCreators;
  let slug;
  if (node.internal.type === `MarkdownRemark`) {
    const fileNode = getNode(node.parent);
    const parsedFilePath = path.parse(fileNode.relativePath);
    if (parsedFilePath.name !== `index` && parsedFilePath.dir !== ``) {
      slug = `/${parsedFilePath.dir}/${parsedFilePath.name}/`;
    } else if (parsedFilePath.dir === ``) {
      slug = `/${parsedFilePath.name}/`;
    } else {
      slug = `/${parsedFilePath.dir}/`;
    }
    // Add slug as a field on the node.
    createNodeField({ node, name: `slug`, value: slug });

    if(node.frontmatter.tags) {
      const tagSlugs = node.frontmatter.tags.map(
        tag => `/tags/${kebabCase(tag)}/`
      );
      createNodeField({ node, name: `tagSlugs`, value: tagSlugs });
    }
  }
};


exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;

  return new Promise((resolve, reject) => {
    const pages = [];
    const blogPost = path.resolve("src/templates/blog-post.js");
    const tagPages = path.resolve("src/templates/tag-page.js");
    // Query for all markdown "nodes" and for the slug we previously created.
    graphql(
      `
        {
          allMarkdownRemark (
            sort: { order: DESC, fields: [frontmatter___date] }
            limit: 1000
          )
          {
            edges {
              node {
                html
                id
                timeToRead
                frontmatter {
                  date(formatString: "MMMM DD, YYYY")
                  path
                  title
                  og_image
                  meta_description
                  tags
                }
              }
            }
          }
        }
      `
    ).then(result => {
      if (result.errors) {
        console.error(result.errors);
        reject(result.errors);
      }

      // Create blog posts pages.
      result.data.allMarkdownRemark.edges.forEach(edge => {
        createPage({
          path: edge.node.frontmatter.path ? edge.node.frontmatter.path : edge.node.field.slug, // required
          component: blogPost,
          context: {
            slug: edge.node.frontmatter.path ? edge.node.frontmatter.path : edge.node.field.slug,
          },
        });
      });

      // Tag Pages
      let tags = [];
      result.data.allMarkdownRemark.edges.forEach(edge=>{
        if(edge.node.frontmatter.tags){
          tags = tags.concat(edge.node.frontmatter.tags);
        }
      });

      tags = [...new Set(tags)];
      tags.forEach(tag => {
        const tagPath = `/tags/${kebabCase(tag)}/`;
        createPage({
          path: tagPath,
          component: tagPages,
          context: {
            tag,
          },
        })
      });
    });

    resolve();
  });
};
